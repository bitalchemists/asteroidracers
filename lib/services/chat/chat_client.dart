library chat_client;

import 'dart:html';
import "dart:async";

import '../../shared/ar_shared.dart';

part 'client/chat_controller.dart';
part 'client/chat_window.dart';
part 'client/message_input_view.dart';
part 'client/username_input_view.dart';
part 'client/view.dart';
