part of ar_shared;

class MessageType {
  static const String CHAT = "CHAT";
  static const String ENTITY = "ENTITY";
  static const String ENTITY_REMOVE = "ENTITY_REMOVE";
  static const String HANDSHAKE = "HANDSHAKE";
  static const String PLAYER = "PLAYER";
  static const String PING_PONG = "PING_PONG";
  static const String COLLISION = "COLLISION";
}