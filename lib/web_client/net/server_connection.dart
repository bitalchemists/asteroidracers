part of web_client;

abstract class ServerConnection implements Connection {    
  Future connect();
}